﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoffeeBeansQualityClasificationUsingRBF.AppCode
{
    class KMeans
    {
        private int jumlahCluster;

        public List<ClusterItem> ListCluster;

        /// <summary>
        /// constructor class KMeans. Inisialisasi jumlahCluster default = 1
        /// </summary>
        public KMeans()
        {
            jumlahCluster = 1;
            ListCluster = new List<ClusterItem>();
        }

        /// <summary>
        /// constructor class KMeans
        /// </summary>
        /// <param name="jumlahCluster">Jumlah cluster yang ingin dibentuk</param>
        public KMeans(int jumlahCluster)
        {
            this.jumlahCluster = jumlahCluster;
            ListCluster = new List<ClusterItem>();
        }

        public void Process(List<DataItem> listDataItem, int panjangData)
        {
            int totalLoop = 1;
            GenerateClusterAwal(panjangData);

            //membuat cluster awal yang berasal dari data yang akan di cluster
            //for (int i = 0; i < jumlahCluster; i++)
            //{
            //    ClusterItem cluster = new ClusterItem(panjangData, i.ToString());
            //    cluster.Data = listDataItem[i].Data;
            //    ListCluster.Add(cluster);
            //}

            List<ClusterItem> tempListCluster = new List<ClusterItem>(ListCluster.Select(x => (ClusterItem)x.Clone()).ToList());
            HitungJarakDataKeCluster(listDataItem);

            while (!checkCluster(ListCluster, tempListCluster) && totalLoop <= 100)
            {
                tempListCluster = new List<ClusterItem>(ListCluster.Select(x => (ClusterItem)x.Clone()).ToList());
                HitungJarakDataKeCluster(listDataItem);
                totalLoop++;
            }
        }

        /// <summary>
        /// Generate cluster awal dengan centroid merupakan bilangan acak 0 atau 1.
        /// </summary>
        /// <param name="panjangData">
        /// Panjang centroid yang akan dibentuk.
        /// </param>
        private void GenerateClusterAwal(int panjangData)
        {
            Random rand = new Random();

            for (int i = 0; i < jumlahCluster; i++)
            {
                ClusterItem cluster = new ClusterItem(panjangData, i.ToString());
                for (int j = 0; j < panjangData; j++)
                {
                    cluster.Data[j] = rand.Next(2);
                }
                ListCluster.Add(cluster);
            }
        }

        /// <summary>
        /// Menghitung jarak setiap data dengan setiap centroid cluster.
        /// Kemudian data akan diklasifikasikan ke setiap cluster berdasarkan nilai jarang terkecil data ke setiap cluster
        /// Setelah penentuan data ke setiap cluster, centroid akan dihitung kembali
        /// </summary>
        /// <param name="listDataItem">
        /// Data yang akan dihitung jaraknya dengan centroid setial cluster
        /// </param>
        private void HitungJarakDataKeCluster(List<DataItem> listDataItem)
        {
            for (int i = 0; i < listDataItem.Count; i++)
            {
                int clusterDistanceIndex = 0;
                for (int j = 0; j < ListCluster.Count; j++)
                {
                    listDataItem[i].JarakCluster[j] = ListCluster[j].EuclideanDistance(listDataItem[i]);
                    if (j == 0)
                    {
                        continue;
                    }
                    else
                    {
                        if (listDataItem[i].JarakCluster[j] < listDataItem[i].JarakCluster[clusterDistanceIndex])
                        {
                            clusterDistanceIndex = j;
                        }
                    }
                }
                listDataItem[i].Cluster = ListCluster[clusterDistanceIndex].ClusterName;
            }

            //generate centroid baru
            for (int i = 0; i < ListCluster.Count; i++)
            {
                var temp = listDataItem.Where(t => t.Cluster.Equals(ListCluster[i].ClusterName)).ToList();
                for (int j = 0; j < ListCluster[i].Data.Length; j++)
                {
                    ListCluster[i].Data[j] = Math.Round((temp.Count <= 0) ? ListCluster[i].Data[j] : temp.Average(t => t.Data[j]), 5);
                }
            }
        }

        /// <summary>
        /// Mengecek apakah nilai kedua cluster sama atau tidak
        /// </summary>
        /// <param name="cluster1">Cluster pertama</param>
        /// <param name="cluster2">Cluster kedua</param>
        /// <returns></returns>
        private bool checkCluster(List<ClusterItem> cluster1, List<ClusterItem> cluster2)
        {
            for (int i = 0; i < cluster1.Count; i++)
            {
                for (int j = 0; j < cluster1[i].Data.Length; j++)
                {
                    if (cluster1[i].Data[j] != cluster2[i].Data[j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    #region
    /// <summary>
    /// Class DataItem adalah class yang digunakan sebagai object dari gambar yang sudah 
    /// mengalami preprocess menjadi bit image (0, 1). Class ini akan digunakan dalam proses
    /// clustering dengan KMeans.
    /// </summary>
    class DataItem
    {
        public double[] Data;
        public double[] JarakCluster;
        public String Cluster;

        /// <summary>
        /// constructor class DataItem
        /// </summary>
        /// <param name="panjangData">Panjang dari data yang akan diterima. Disesuaikan dengan dimensi gambar h x w</param>
        /// <param name="jumlahCluster">Jumlah cluster yang diinginkan dalam proses KMeans. 
        /// Berguna untuk inisialisasi array yang menyimpan jarak DataItem dengan setiap cluster</param>
        public DataItem(int panjangData, int jumlahCluster)
        {
            Data = new double[panjangData];
            JarakCluster = new double[jumlahCluster];
            Cluster = "";
        }
    }
    #endregion

    #region
    /// <summary>
    /// Class yang digunakan sebagai object dari cluster yang dibentuk
    /// </summary>
    class ClusterItem : ICloneable
    {

        public double[] Data;
        public string ClusterName;

        /// <summary>
        /// constructor class ClusterItem
        /// </summary>
        /// <param name="panjangData">Panjang dari centroid yang akan diterima. 
        /// Digunakan sebagai panjang array dalam inisialisasi array yang menampung data centroid.
        /// Panjang data harus sama dengan panjang data yang digunakan pada DataItem</param>
        /// <param name="clusterName">
        /// Nama cluster
        /// </param>
        public ClusterItem(int panjangData, string clusterName)
        {
            Data = new double[panjangData];
            ClusterName = clusterName;
        }

        /// <summary>
        /// Fungsi untuk menghitung jarak cluster dengan DataItem. Fungsi yang digunakan dalam perhitungan
        /// adalah fungsi Euclidean.
        /// </summary>
        /// <param name="item">
        /// Object DataItem yang akan dihitung jaraknya dengan centroid cluster. Overide function EuclideanDistance(double[] data)
        /// </param>
        /// <returns>Jarak data dengan centroid cluster</returns>
        public double EuclideanDistance(DataItem item)
        {
            return EuclideanDistance(item.Data);
        }

        /// <summary>
        /// Fungsi untuk menghitung jarak cluster dengan data. 
        /// Fungsi yang digunakan adalah fungsi Euclidean
        /// </summary>
        /// <param name="data">
        /// Array of double, data yang dihitung jaraknya dengan centroid
        /// </param>
        /// <returns>Jarak data dengan centroid cluster</returns>
        public double EuclideanDistance(double[] data)
        {
            double distance = 0.0d;
            for (int i = 0; i < Data.Length; i++)
            {
                distance += Math.Pow(data[i] - this.Data[i], 2);
            }
            distance = Math.Sqrt(distance);

            return distance;
        }

        /// <summary>
        /// Fungsi untuk melakukan clone list cluster menjadi reference baru
        /// </summary>
        /// <param name="listcluster">List cluster yang akan diclone</param>
        /// <returns></returns>
        public static List<ClusterItem> Clone(List<ClusterItem> listcluster)
        {
            List<ClusterItem> cluster = new List<ClusterItem>();
            foreach (ClusterItem item in listcluster)
            {
                cluster.Add((ClusterItem)item.MemberwiseClone());
            }
            return cluster;
        }

        

        //public static IList<T> Clone<T>(IList<T> listToClone) where T : ICloneable
        //{
        //    return listToClone.Select(item => (T)item.Clone()).ToList();
        //}

        #region ICloneable Members

        public object Clone()
        {
            ClusterItem item = new ClusterItem(Data.Length, ClusterName);
            for (int i = 0; i < Data.Length; i++)
            {
                item.Data[i] = Data[i];
            }
            return item;
        }

        #endregion
    }
    #endregion

    public static class CloneableExtension
    {
        public static IList<T> Clone<T>(this IList<T> list) where T : ICloneable
        { return list.Select(item => (T)item.Clone()).ToList(); }
    }
}
