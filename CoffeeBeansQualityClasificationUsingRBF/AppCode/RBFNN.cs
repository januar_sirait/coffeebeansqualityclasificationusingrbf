﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace CoffeeBeansQualityClasificationUsingRBF.AppCode
{
    /// <summary>
    /// Radial Basis Function Neural Network Class
    /// </summary>
    class RBFNN
    {
        public int TotalInput { get; set; }
        public int TotalHidden { get; set; }
        public int TotalOutput { get; set; }
        public double Spread { get; set; }

        public List<DataItem> Input { get; set; }
        public List<ClusterItem> Hidden { get; set; }
        public double[][] Output { get; set; }
        public double[,] OutputTarget { get; set; }
        public double[][] Weight { get; set; }

        public StringBuilder sb;

        public RBFNN()
        { }

        /// <summary>
        /// Constuctor RBFNN
        /// </summary>
        /// <param name="input">Total input will initialized</param>
        /// <param name="hidden">Total hidden will initialized</param>
        public RBFNN(int input, int hidden)
        {
            TotalInput = input;
            TotalHidden = hidden;
        }


        /// <summary>
        /// Fungsi untuk menginisialisasi nilai Spread. Spread = jarak maksimum antara 2 pusat dibagi akar banyak pusat
        /// </summary>
        public void SetSpread() {
            if (Hidden.Count == 0)
            {
                throw new Exception("Can't calculate spread value. Hidden neuron is null.");
            }
            else if (Hidden.Count == 1)
            {
                Spread = 0.5;
            }
            else
            {
                double distance = 0.0d;
                for (int i = 0; i < Hidden.Count-1; i++)
                {
                    for (int j = i + 1; j < Hidden.Count; j++)
                    {
                        double temp = Hidden[i].EuclideanDistance(Hidden[j].Data);
                        distance = (temp > distance) ? temp : distance;
                    }
                }

                Spread = distance / Math.Sqrt(Hidden.Count);
            }
        }

        public double GetXGausianValue(double x, double[] centroid)
        {
            double temp = 0.0d;
            foreach (double item in centroid)
            {
                temp += Math.Pow(x - item, 2);
            }

            temp = temp / (2 * Math.Pow(Spread, 2));
            return Math.Exp(-1 * temp);
        }

        public double[,] GetGausianMatrix(double[] input)
        {
            double[,] matrix = new double[input.Length, Hidden.Count];

            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < Hidden.Count; j++)
                {
                    matrix[i, j] = GetXGausianValue(input[i], Hidden[j].Data);
                }

                //code ini untuk menambahkan nilai bias ke dalam matrix gausian. Default value = 1
                //matrix[i, Hidden.Count] = 1;
            }
            return matrix;
        }

        public double[] CalculateHiddenValue(double[] input, double[,] gausianMatrix)
        {
            double[] hiddenValue = new double[TotalHidden];

            for (int i = 0; i < TotalHidden; i++)
            {
                double temp = 0;
                for (int j = 0; j < input.Length; j++)
                {
                    temp += (input[j] * gausianMatrix[j, i]);
                }

                temp = 1 / (1 + Math.Exp(-1 * temp));
                //temp = (temp > 0.6) ? 1 : 0;
                hiddenValue[i] = temp;
            }

            return hiddenValue;
        }

        public void Training()
        {
            double[][] output = new double[Input.Count][];
            for (int i = 0; i < Input.Count; i++)
            {
                double[,] gausian = GetGausianMatrix(Input[i].Data);
                double[] hiddenValue = CalculateHiddenValue(Input[i].Data, gausian);

                // menghitung bobot baru(W) dengan mengalikan pseudoinverse dari matrix G
                // dengan verktor target dari data training
                Matrix gausianMatrix = DenseMatrix.OfArray(gausian);
                Matrix targetMatrix = DenseMatrix.OfArray(OutputTarget);

                Matrix<double> weightMatrix = (gausianMatrix.Transpose() * gausianMatrix).Inverse() * gausianMatrix.Transpose() * targetMatrix;
                double[][] weight = weightMatrix.ToColumnArrays();
                Weight = weight;

                //hitung output
                output[i] = new double[TotalOutput];
                for (int j = 0; j < TotalOutput; j++)
                {
                    double net = 0.0d;
                    for (int k = 0; k < hiddenValue.Length; k++) {
                        net += hiddenValue[k] * weight[j][k];
                    }
                    output[i][j] = 1 / (1 + Math.Exp(-1 * net)); ;
                }
            }

            Output = output;
        }
    }
}
