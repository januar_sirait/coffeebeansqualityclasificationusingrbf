﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using Newtonsoft.Json;

using CoffeeBeansQualityClasificationUsingRBF.AppCode;

namespace CoffeeBeansQualityClasificationUsingRBF
{
    public partial class Form1 : Form
    {
        const int PICTURE_WIDTH = 120;
        const int RESIZE = 30;

        double[][] dataInput;
        double[][] dataOutput;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            dataInput = new double[openFileDialog1.FileNames.Count()][];
            dataOutput = new double[openFileDialog1.FileNames.Count()][];
            int i = 0;
            int locationX = 10;
            int locationY = 10;
            int margin = 10;

            Bitmap originalImage;
            ResizeBicubic resizeFilter = new ResizeBicubic(RESIZE, RESIZE);

            Bitmap resizeImage;
            Bitmap grayscaleBitmap;

            Threshold filter = new Threshold(180);
            Bitmap thresholdBitmap;

            SobelEdgeDetector sobelFilter = new SobelEdgeDetector();
            Bitmap sobelImage;

            foreach (Control control in panel1.Controls)
            {
                panel1.Controls.Remove(control);
            }

            foreach (String filename in openFileDialog1.FileNames)
            {
                PictureBox pictureBoxOriginal = GetPictureBox("original" + i, new System.Drawing.Point(locationX, locationY));
                panel1.Controls.Add(pictureBoxOriginal);
                originalImage = (Bitmap)Bitmap.FromFile(filename);
                
                resizeImage = resizeFilter.Apply(originalImage);
                pictureBoxOriginal.Image = originalImage;

                locationX += margin + PICTURE_WIDTH;
                PictureBox pictureBoxGray = GetPictureBox("gray" + i, new System.Drawing.Point(locationX, locationY));
                panel1.Controls.Add(pictureBoxGray);
                grayscaleBitmap = Grayscale.CommonAlgorithms.BT709.Apply(resizeImage);
                pictureBoxGray.Image = grayscaleBitmap;

                locationX += margin + PICTURE_WIDTH;
                PictureBox pictureBoxThreshold = GetPictureBox("threshold" + i, new System.Drawing.Point(locationX, locationY));
                panel1.Controls.Add(pictureBoxThreshold);
                thresholdBitmap = filter.Apply(grayscaleBitmap);
                pictureBoxThreshold.Image = thresholdBitmap;

                locationX += margin + PICTURE_WIDTH;
                PictureBox pictureBoxSobel = GetPictureBox("sobel" + i, new System.Drawing.Point(locationX, locationY));
                panel1.Controls.Add(pictureBoxSobel);
                sobelImage = sobelFilter.Apply(thresholdBitmap);
                pictureBoxSobel.Image = sobelImage;
                
                dataInput[i] = new double[sobelImage.Height * sobelImage.Width];
                dataOutput[i] = new double[1];
                if (i % 2 == 0)
                {
                    dataOutput[i][0] = 1;
                }
                else {
                    dataOutput[i][0] = 0;
                }

                for (int y = 0; y < sobelImage.Height; y++)
                {
                    for (int x = 0; x < sobelImage.Width; x++)
                    {
                        dataInput[i][x * sobelImage.Width + y] = ((sobelImage.GetPixel(x, y).R > 0) ? 1 : 0);
                    }
                }

                locationX = 10;
                locationY += margin + PICTURE_WIDTH;
                i++;
            }
        }

        private PictureBox GetPictureBox(String name, System.Drawing.Point location)
        {
            PictureBox pictureBox = new PictureBox();
            pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pictureBox.Location = location;
            pictureBox.Name = name;
            pictureBox.Size = new System.Drawing.Size(PICTURE_WIDTH, PICTURE_WIDTH);
            pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBox.TabIndex = 0;
            pictureBox.TabStop = false;

            return pictureBox;
        }

        private void btnTraning_Click(object sender, EventArgs e)
        {
            List<DataItem> inputData = new List<DataItem>();
            foreach (var data in dataInput)
            {
                DataItem dataItem = new DataItem(data.Length, 2);
                dataItem.Data = data;

                inputData.Add(dataItem);
            }
            //clustering
            KMeans kmeans = new KMeans(2);
            kmeans.Process(inputData, inputData[0].Data.Length);

            string clusterJson = JsonConvert.SerializeObject(kmeans.ListCluster);
            WriteToFile("data/cluster.json", clusterJson);

            double[,] target = new double[inputData[0].Data.Length, 1];
            using (StreamReader sr = new StreamReader("data/target.json"))
            {
                double[] temp = JsonConvert.DeserializeObject<double[]>(sr.ReadToEnd());
                for (int i = 0; i < temp.Length; i++)
                {
                    target[i, 0] = temp[i];
                }
            }

            RBFNN rbf = new RBFNN();
            rbf.TotalInput = inputData[0].Data.Length;
            rbf.TotalHidden = 2;
            rbf.TotalOutput = 1;
            rbf.Input = inputData;
            rbf.Hidden = kmeans.ListCluster;
            rbf.OutputTarget = target;
            rbf.SetSpread();
            rbf.Training();
            //txtMessage.Text = rbf.sb.ToString();

            String outputJson = JsonConvert.SerializeObject(rbf.Output);
            WriteToFile("data/output.json", outputJson);

            String weight = JsonConvert.SerializeObject(rbf.Weight);
            WriteToFile("data/weight.json", weight);

            MessageBox.Show("Training finish", "Success", MessageBoxButtons.OK);

            txtMessage.Text = "Botot = " + weight;
            txtMessage.Text += "\r\nOutput = " + outputJson;
        }


        private void WriteToFile(String filename, String data)
        {
            using (StreamWriter sw = new StreamWriter(filename))
            {
                sw.Write(data);
            }
        }
    }
}
